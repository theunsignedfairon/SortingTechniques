import java.util.ArrayList;

/**
 * Implementation of Quick Sort
 */
public class QuickSort {
    private static void sort(ArrayList<Integer> array, int low, int high) {
        int i = low;
        int j = high;
        int mid = array.get(low + (high - low) / 2);    // Mid of array

        do {
            while (array.get(i) < mid) ++i;             // Continue while i-th less than mid
            while (array.get(j) > mid) --j;             // Continue while j-th greater then mid
            if (i <= j) {                               // Swap elements
                SupportMethods.swap(array, i, j);
                i++;
                j--;
            }
        } while (i <= j);

        if (low < j) sort(array, low, j);               // Recursion call
        if (i < high) sort(array, i, high);             // Recursion call
    }


    /**
     * User interface
     * @param array input array
     */
    public static void quickSort(ArrayList<Integer> array) {
        sort(array, 0, array.size() - 1);
    }
}
