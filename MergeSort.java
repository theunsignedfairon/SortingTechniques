import java.util.ArrayList;

/**
 * Implementation of Merge Sort
 */
public class MergeSort {
    /**
     * User Interface
     * @param array input array
     * @return return sorted array
     */
    public static ArrayList<Integer> mergeSort(ArrayList<Integer> array) {
        if (array.size() <= 1) return array;    // If array contains 1 or 0 elements then it has already been sortered
        int mid = array.size() / 2;             // Middle element
        return merge(mergeSort(SupportMethods.subArrayList(array, 0, mid)), mergeSort(SupportMethods.subArrayList(array, mid, array.size())));
    }


    private static ArrayList<Integer> merge(ArrayList<Integer> array1, ArrayList<Integer> array2) {
        int i = 0, j = 0;
        ArrayList<Integer> result = new ArrayList<Integer>();

        for (int index = 0; index < array1.size() + array2.size(); index++) {
            if (i < array1.size() && j < array2.size()) {       // Put the least element from two current elements from both array
                if (array1.get(i) < array2.get(j)) {
                    result.add(array1.get(i));
                    i++;
                } else {
                    result.add(array2.get(j));
                    j++;
                }
            } else if (i < array1.size()) {         // If 2nd array is over, put elements from 1st array
                result.add(array1.get(i));
                i++;
            } else {
                result.add(array2.get(j));          // If 1st array is over, put elements from 2nd array
                j++;
            }
        }
        return result;
    }
}
