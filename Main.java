import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        // Declaration
        Scanner sc = new Scanner(new FileReader("input.txt"));                  // Scanner for input data
        PrintWriter wr = new PrintWriter("output.txt");                         // Writer for output data

        ArrayList<String> inputSequence;                                                // Input string
        ArrayList<Integer> intArray = new ArrayList<>();                                // Array of integer elements
        ArrayList<Integer> charArray = new ArrayList<>();                               // Array of character(ASCII-Codes) elements
        ArrayList<Integer> pos = new ArrayList<>();                                     // Array for storing type of i-th element
        ArrayList<ArrayList<String>> arrayOfResults;                                    // Array for storing results of different sorting techniques

        // Scan input data
        inputSequence = new ArrayList<>(Arrays.asList(sc.nextLine().split(" ")));

        // Separate input data on two arrays and store type of elements in 'pos'
        // If Integer then put '1' otherwise put '0'
        for (int i = 0; i < inputSequence.size(); i++) {
            if (SupportMethods.isNumber(inputSequence.get(i))) {
                intArray.add(Integer.parseInt(inputSequence.get(i)));
                pos.add(1);
            } else {
                charArray.add((int)inputSequence.get(i).charAt(0));
                pos.add(0);
            }
        }

        // Store result of different sorting techniques
        arrayOfResults = doDiffSorts(intArray, charArray, pos);


        // Compare all result and if there are the same print result otherwise print 'ERROR'
        if (compareResults(arrayOfResults)) {
            for (int i = 0; i < arrayOfResults.get(0).size(); i++) {
                wr.print(arrayOfResults.get(0).get(i) + " ");
            }
        } else {
            wr.print("ERROR");
        }

        // Close Input/Output streams
        sc.close();
        wr.close();
    }


    /**
     * Merge two arrays in one
     * @param intArray 1st input array
     * @param charArray 2nd input array
     * @param pos rule for merging
     * @return return array
     */
    public static ArrayList<String> merging(ArrayList<Integer> intArray, ArrayList<Integer> charArray, ArrayList<Integer> pos) {
        int intCarriage = 0, charCarriage = 0;
        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < intArray.size() + charArray.size(); i++) {
            if (pos.get(i) == 1) {
                result.add(String.valueOf(intArray.get(intCarriage)));
                intCarriage++;
            } else if (pos.get(i) == 0) {
                result.add(String.valueOf(Character.toChars(charArray.get(charCarriage))));
                charCarriage++;
            }
        }
        return result;
    }


    /**
     * Sort arrays using different techniques
     * @param intArray array of Integers
     * @param charArray array of Characters
     * @param pos rule for merging
     * @return return sorted array
     */
    public static ArrayList<ArrayList<String>> doDiffSorts(ArrayList<Integer> intArray, ArrayList<Integer> charArray, ArrayList<Integer> pos) {
        ArrayList<ArrayList<String>> result = new ArrayList<>();
        ArrayList<Integer> arr1 = new ArrayList<>(intArray);
        ArrayList<Integer> arr2 = new ArrayList<>(charArray);

        BubbleSort.bubbleSort(arr1); BubbleSort.bubbleSort(arr2);
        result.add(merging(arr1, arr2, pos));

        arr1 = new ArrayList<>(intArray); arr2 = new ArrayList<>(charArray);
        InsertionSort.insertionSort(arr1); InsertionSort.insertionSort(arr2);
        result.add(merging(arr1, arr2, pos));

        arr1 = new ArrayList<>(intArray); arr2 = new ArrayList<>(charArray);
        SelectionSort.selectionSort(arr1); SelectionSort.selectionSort(arr2);
        result.add(merging(arr1, arr2, pos));

        arr1 = new ArrayList<>(intArray); arr2 = new ArrayList<>(charArray);
        QuickSort.quickSort(arr1); QuickSort.quickSort(arr2);
        result.add(merging(arr1, arr2, pos));

        arr1 = new ArrayList<>(intArray); arr2 = new ArrayList<>(charArray);
        arr1 = MergeSort.mergeSort(arr1); arr2 = MergeSort.mergeSort(arr2);
        result.add(merging(arr1, arr2, pos));

        return result;
    }


    /**
     * Compare result and if there are the same return True otherwise False
     * @param results result of sorting
     * @return return True/False
     */
    public static boolean compareResults(ArrayList<ArrayList<String>> results) {
        for (int i = 0; i < results.size() - 1; i++) {
            for (int j = 1; j < results.size(); j++) {
                if (!isEqualArrays(results.get(j), results.get(i)))
                    return false;
            }
        }
        return true;
    }


    /**
     * Compare two arrays on equality
     * @param arr1 1st input array
     * @param arr2 2nd input array
     * @return return True/False
     */
    public static boolean isEqualArrays(ArrayList<String> arr1, ArrayList<String> arr2) {
        if (arr1.size() != arr2.size()) return false;
        for (int i = 0; i < arr1.size(); i++) {
            if (!arr1.get(i).equals(arr2.get(i))) {
                return false;
            }
        }
        return true;
    }
}
