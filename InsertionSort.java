import java.util.ArrayList;

/**
 * Implementation of Insertion Sort
 */
public class InsertionSort {
    public static void insertionSort(ArrayList<Integer> array) {
        for (int i = 1; i < array.size(); i++) {
            int value = array.get(i);
            int j;
            for (j = i - 1; j >= 0 && array.get(j) > value; j--)
                array.set(j + 1, array.get(j));
            array.set(j + 1, value);
        }
    }
}
