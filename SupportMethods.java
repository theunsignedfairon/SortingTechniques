import java.util.ArrayList;

/**
 * Library with some supporting methods
 */
public class SupportMethods {


    /**
     * Swap two elements of array
     * @param array input array
     * @param i index of 1st swapping element
     * @param j index of 2nd swapping element
     */
    public static void swap(ArrayList<Integer> array, int i, int j) {
        int buf = array.get(i);
        array.set(i, array.get(j));
        array.set(j, buf);
    }


    /**
     * Get part of ArrayList from start to end - 1
     * @param array input array
     * @param start 1st index
     * @param end last index but not included
     * @return return ArrayList
     */
    public static ArrayList<Integer> subArrayList(ArrayList<Integer> array, int start, int end) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = start; i < end; i++) {
            result.add(array.get(i));
        }
        return result;
    }


    /**
     * Check: Is string number or not
     * @param string input string
     * @return return True/False
     */
    public static boolean isNumber(String string) {
        try{
            Integer.parseInt(string);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
