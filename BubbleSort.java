import java.util.ArrayList;


/**
 * Implementation of Bubble Sort
 */
public class BubbleSort {
    public static void bubbleSort(ArrayList<Integer> array) {
        for (int i = 0; i < array.size() - 1; i++) {                    // Outloop
            for (int j = 1; j < array.size(); j++) {                    // Inloop
                if (array.get(j) < array.get(j - 1))                    // If current element less than previous than swap them otherwise continue
                    SupportMethods.swap(array, j, j - 1);
            }
        }
    }
}
